from xmlrpclib import ServerProxy
import hashCheck, os
import Tkinter as tk
from tkFileDialog import askopenfilename
import urllib, gzip, sys
from string import strip
from tkMessageBox import askquestion

from colorama import init
init() # initiaon for the "colorama" module
from colorama import Fore, Style

# change window title
try:
	import ctypes
	ctypes.windll.kernel32.SetConsoleTitleA('EasySub v1')
except ImportError:
	pass 

server = "http://api.opensubtitles.org/xml-rpc"

# this class processess all things related to subtitle and movie files
class FileProcessor(object):

	def __init__(self):
		self.file_path = None

	def GetFilePath(self):
		# asks for file path with the file selector windows
		root = tk.Tk()
		root.withdraw()
		if not self.file_path:
			self.file_path = askopenfilename(title="Select movie file")
			if not self.file_path:
				print (Fore.RED + Style.BRIGHT + "\nFile Path is invalid...")
				raw_input("Press any key to exit")
				sys.exit(1)
			print (Fore.GREEN + "File: " + self.file_path)
			print 
		return self.file_path

	def LoginData(self):
		# to access the OpenSub api, every user should have an account,
		# the account credentials are stored in the folder Login.txt,
		# this function retrieves the data from the said file
		with open("Login.txt", "r") as f:
			x = f.readlines()
			x1 = x[0].strip() # strip function is used to avoid Newlines
			x2 = x[1].strip()
			LoginInfo = x1[x1.index("=")+1:], x2[x2.index("=")+1:] # only selects the texts which are after the equal sign, check Login.txt
			if LoginInfo[0] == '' or LoginInfo[1] == '':
				LoginInfo = None, None # Anonymous login is supported, it executes if no Logindata is provided
				# print a warning message if logindata not found, because it may result Status Code 401 Unauthorized
				print (Fore.YELLOW + "WARNING: This program requires your userinfo to work properly. Please provide your login credentials in Login.txt\nTrying anonymous login\n")
		if not f.closed: f.close()
		return LoginInfo

	def GetHash(self):
		self.file_path = self.GetFilePath()
		return hashCheck.hash(self.file_path)

	def ProcessSub(self, Format):
		# unpacks subtitle file, places it in the movie folder, and renames it according to the movie name
		print (Fore.WHITE + "Unpacking subtitle file...\n")
		Path = self.GetFilePath()
		movieFile = os.path.basename(Path)
		subFileName = os.path.splitext(Path)[0] + "." + Format

		with gzip.open("sub.gz", "rb") as comp:
			content = comp.read()
			with open(subFileName, "wb") as f:
				f.write(content)
			if not f.closed: f.close()
		if not comp.closed: comp.close()
		self.CleanUp()
		print (Fore.GREEN + Style.BRIGHT + "Subtitle file is downloaded in the movie folder. Enjoy!\n\n")
		Ask = askquestion("Confirmation", "Do you want to download subtitle for another movie?", icon="info")
		if Ask == "yes":
			return True
		else:
			return False
		
	def CleanUp(self):
		os.remove("sub.gz")

		
# the main class to download subtitles using api
class MainEngine(object):

	def __init__(self, language="None"): 
		self.rpc = ServerProxy(server, allow_none=True) # allow_none is mandatory to make some methods work
		self.user_agent = "muhib96" # my registered user agent for opensub api
		self.File = FileProcessor() # a variable needed to access the FileProcessor class
		self.Start()

	def getToken(self):
		# function to login the user
		try:
			return self.Token
		except AttributeError:
			print (Fore.WHITE + "Logging in...")
			data = self.File.LoginData()
			User = data[0]
			Pass = data[1]
			self.logindata = self.rpc.LogIn(User, Pass, "eng", self.user_agent)
			# print self.logindata
			self.Token = self.logindata["token"]
			return self.Token

	def logout(self):
		# function to logout
		self.rpc.LogOut(self.Token)
		print (Fore.WHITE + "Logout success")
		raw_input("Press any key to exit")

	def subSearch(self, path):
		# api processing
		self.hash = self.File.GetHash()
		token = self.getToken() # user gets logged in
		print (Fore.WHITE + "Please wait...")
		self.param = [
			token, # token
			{
				'sublanguageid' : 'eng', # sublanguageid
				'moviehash' : self.hash, #hash
				'moviesize' : os.path.getsize(path), # byte size 
			}
		]
		Obj = self.rpc.SearchSubtitles(token, self.param)
		if not Obj['data']:
			print (Fore.RED + Style.BRIGHT + "Subtitle not found for this file...")
			self.logout()
			raw_input("\nPress any key to exit")
			sys.exit(0)
		
		# gets the data related to downloaing the subtitle
		self.Download = Obj['data'][0]['SubDownloadLink']
		self.subFormat = Obj['data'][0]['SubFormat']

		# subtitle download
		self.DownloadSub()

	def DownloadSub(self):
		print (Fore.WHITE + "Downloading Subtitle...")
		opener = urllib.URLopener()
		opener.retrieve(self.Download, "sub.gz")
		print (Fore.WHITE + "\nFile Downloaded")
		Val = self.File.ProcessSub(self.subFormat) # True or False

		# checks if the user wants to download another movie subtitle
		if Val:
			self.File = FileProcessor()
			# print "File Path: %s" % self.File.GetFilePath()
			self.subSearch(self.File.GetFilePath())
		else:
			self.logout()

	def Start(self):
		try:
			self.subSearch(self.File.GetFilePath())
		except Exception, e:
			print(Fore.RED + Style.BRIGHT + "An unexpected error occured: %s" % e)
			try:
				self.logout()
			except:
				pass
			raw_input("Press any key to exit")
			sys.exit(1)

def main():
	print (Fore.CYAN + Style.BRIGHT + "# EasySub v0.1")
	print "# Developed by Sadman Muhib Samyo"
	print "# email: ahmedsadman.211@gmail.com"
	print 
	print (Fore.WHITE + "Select your movie file")
	MainEngine()

if __name__ == '__main__':
	main()
